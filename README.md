# DummyLB

`dummylb` is a simple kubernetes controller to set the status on LoadBalancer Services to pretend they were successfully provisioned.
This is only to be used for testing things that require the Service to be _ready_ such as helm's `--wait` flag.

## What does this controller do?

Not much. It watches for events related to lifetime of services and sets the status if they're of the type, `LoadBalancer`.

## Building

You need `go` version 1.11 or greater with mod enabled.

Run:

```bash
# to build the whole thing
make
```

## Running

Make sure your `kubectl` is working.

### Running as standalone binary

Just run `./dummylb-controller`.

### Running as pod in a cluster

* set `DOCKER_REPO` variable to point to a docker registry where you can store the image
* run `make build-image DOCKER_REPO=myrepo.dom/dummylb` to build locally a docker image with your controller
* run `make push-image DOCKER_REPO=myrepo.dom/dummylb` to push it to the registry
* run `make deploy DOCKER_REPO=myrepo.dom/dummylb` to deploy to kubernetes
* run `make undeploy` to delete controller from kubernetes
